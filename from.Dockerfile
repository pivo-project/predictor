FROM eclipse-temurin:11.0.18_10-jdk-focal

WORKDIR /app

# Install Scala & SBT
RUN apt-get update
RUN apt-get install apt-transport-https curl gnupg -yqq
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list
RUN curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" |  gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import
RUN chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg
RUN apt-get update
RUN apt-get install sbt -yqq
RUN sbt -V

# Install docker
RUN apt-get update
RUN apt-get install tar -yqq
RUN curl https://download.docker.com/linux/static/stable/x86_64/docker-23.0.0.tgz > docker.tar.gz
RUN tar xzvf docker.tar.gz
RUN cp docker/* /usr/bin/
RUN dockerd &
RUN rm -rf docker*