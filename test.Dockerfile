FROM registry.gitlab.com/pivo-project/predictor:from

WORKDIR /app
COPY . .

CMD ["sbt", "clean", "test"]
