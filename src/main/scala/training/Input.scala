package training

import cats.effect.IO
import org.http4s.EntityDecoder
import org.http4s.circe.jsonOf
import io.circe.generic.auto._

object Input:
  case class Input(image: String)

  implicit val decoder: EntityDecoder[IO, Input] = jsonOf[IO, Input]
end Input

