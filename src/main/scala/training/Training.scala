package training

import cats.effect.IO
import io.circe.{Encoder, Json}
import io.circe.literal.json
import io.circe.syntax.*
import scala.util.chaining._
import io.circe.generic.auto._

object Training:
  case class Training(message: String)

  def fromString(response: String): IO[Json] = response
    .pipe(Training.apply)
    .asJson
    .pipe(IO.pure)

end Training

