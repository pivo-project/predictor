package common

import cats.effect.IO

import java.io.{BufferedOutputStream, File, FileOutputStream}
import java.nio.file.{Files, Path, Paths}
import java.security.MessageDigest

object FileSystem:
	private def makeFolder(): IO[Path] = IO(Files.createDirectories(Paths.get("temp")))

	def writeBytes(data: Array[Byte], file: File): IO[Unit] =
		val target = new BufferedOutputStream(new FileOutputStream(file))

		IO(try data.foreach(target.write(_)) finally target.close())
	end writeBytes  

	def makeFile(data: String): IO[File] = 
		val fileName = MessageDigest.getInstance("SHA-256")
			.digest(data.getBytes("UTF-8"))
			.map("%02x".format(_)).mkString

		for 
			_ <- makeFolder()
			file <- IO(new File(s"""temp/$fileName"""))
		yield file
	end makeFile

	def removeFile(fileName: String): IO[Unit] = for 
		_ <- IO(new File(fileName).delete())
		unit <- IO.unit
	yield unit
end FileSystem

