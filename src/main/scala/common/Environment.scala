package common

import cats.effect.IO

object Environment:
	trait RuntimeEnvironment[T]:
		protected def getFromEnvironment(key: String): IO[String] =
			sys.env.get(key) match
				case Some(value) => IO.pure(value)
				case None => IO.raiseError(new InternalError(s"Int env variable $key not set"))
		end getFromEnvironment

		def get(key: String): IO[T]
	end RuntimeEnvironment

	implicit object IntEnvironment extends RuntimeEnvironment[Int]:
		def get(key: String): IO[Int] = for
			stringValue <- getFromEnvironment(key)
		yield stringValue.toInt
	end IntEnvironment

	implicit object StringEnvironment extends RuntimeEnvironment[String]:
		def get(key: String): IO[String] = for
			stringValue <- getFromEnvironment(key)
		yield stringValue
	end StringEnvironment

	def getValue[T](key: String)(implicit runtimeEnvironment: RuntimeEnvironment[T]): IO[T] =
		runtimeEnvironment.get(key)
	end getValue
end Environment
