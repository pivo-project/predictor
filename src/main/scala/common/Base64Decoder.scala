package common

import cats.effect.IO

import java.io.{BufferedOutputStream, File, FileOutputStream}
import java.security.MessageDigest
import java.util.Base64

object Base64Decoder:
	def decode(data: String): IO[Array[Byte]] = IO(Base64.getDecoder.decode(data))
end Base64Decoder

