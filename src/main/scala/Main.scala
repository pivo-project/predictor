import categories.Category
import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.*
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.server.middleware.*
import startup.{ErrorHandler, HttpService, Network}

object Main extends IOApp:
	private def runServer(host: Host, port: Port): IO[ExitCode] = EmberServerBuilder
		.default[IO]
		.withHost(host)
		.withPort(port)
		.withHttpApp(GZip(HttpService.service))
		.withErrorHandler(ErrorHandler.handler)
		.build
		.use(_ => IO.never)
		.as(ExitCode.Success)


	def run(args: List[String]): IO[ExitCode] = for
		_ <- Category.ensureDefault
		host <- Network.getHost
		port <- Network.getPort
		exitCode <- runServer(host, port)
	yield exitCode
end Main

