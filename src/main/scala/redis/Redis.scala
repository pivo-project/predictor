package redis

import cats.effect.{IO, Resource}
import common.Environment
import redis.clients.jedis.JedisPooled
import redis.clients.jedis.json.Path

object Redis:
	private def getHost = Environment.getValue[String]("REDIS_HOST")

	private def getPort = Environment.getValue[Int]("REDIS_PORT")

	private def pool = for
		host <- getHost
		port <- getPort
	yield new JedisPooled(host, port)

	def get[T](key: String): IO[Option[T]] = for
		poolInstance <- pool
		reference <- IO(poolInstance.jsonGet(key))
		casted <- IO(Option(reference.asInstanceOf[T]))
	yield casted

	def set[T](key: String, value: T, path: Path = Path.ROOT_PATH): IO[String] = for
		poolInstance <- pool
		reference <- IO(poolInstance.jsonSet(key, path, value))
	yield reference

	def append[T](key: String, value: T, path: Path = Path.ROOT_PATH): IO[Unit] = for
		poolInstance <- pool
		_ <- IO(poolInstance.jsonArrAppend(key, path, value))
	yield IO.unit
end Redis
