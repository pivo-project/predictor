package docker

enum Images(val name: String):
	case Predict extends Images("predict")
	case Train extends Images("train")
end Images

