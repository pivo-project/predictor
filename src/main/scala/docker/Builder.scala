package docker

import cats.effect.IO
import scala.util.chaining.*

object Builder:
	private def buildArgList = "docker"
		:: "exec"
		:: "tensorflow"
		:: Nil

	def buildCommand: IO[String] = for
		argList <- buildArgList pipe IO.pure
		command <- argList.mkString(" ") pipe IO.pure
	yield command
end Builder

