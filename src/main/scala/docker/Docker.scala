package docker

import cats.effect.IO
import common.{Base64Decoder, FileSystem}
import python.Functions
import python.System
import sys.process.*
import image.Image

object Docker:
	private def getPythonFunction(image: Images): Functions = image match
		case Images.Predict => Functions.Predict
		case Images.Train => Functions.Train

	private def runCommand(docker: String, python: String): IO[String] = IO(s"$docker $python".!!)
		.map(rawResult => rawResult.split("\n").last)
	
	def run(imagePath: String, image: Images): IO[String] = for
		file <- FileSystem.makeFile(imagePath)
		decodedImage <- Base64Decoder.decode(imagePath)
		_ <- FileSystem.writeBytes(decodedImage, file)
		_ <- Image.grayscale(file)
		dockerCommand <- Builder.buildCommand
		pythonCommand <- System.buildCommand(getPythonFunction(image), file.getPath)
		result <- runCommand(dockerCommand, pythonCommand)
	yield result
end Docker
