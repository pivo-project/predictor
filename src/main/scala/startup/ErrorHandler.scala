package startup

import cats.effect.IO
import io.circe.{Json, ParsingFailure}
import io.circe.literal.json
import org.http4s.Response
import org.http4s.dsl.io.*
import org.http4s.circe.*

import scala.util.chaining.*

object ErrorHandler:
	private def buildMessage(error: Throwable) = json"""{"error":  ${error.getMessage}}"""

	private def castErrorType(error: Throwable)(message: Json) = error match
		case _: InternalError  => InternalServerError(message)
		case _: Exception => BadRequest(message)

	val handler: PartialFunction[Throwable, IO[Response[IO]]] = error => error
		.pipe(buildMessage)
		.pipe(castErrorType(error))

end ErrorHandler

