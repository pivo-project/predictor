package startup

import cats.effect.IO
import com.comcast.ip4s.*
import common.Environment

object Network:
  private def castVariable[F](option: Option[F]) = option match
    case Some(value) => IO.pure(value)
    case None => IO.raiseError(new Exception("Env variable malformed"))

  def getHost: IO[Host] = for
    rawHost <- Environment.getValue[String]("HOST")
    host <- castVariable[Ipv4Address](Ipv4Address.fromString(rawHost))
  yield host

  def getPort: IO[Port] = for
    rawPort <- Environment.getValue[Int]("PORT")
    port <- castVariable[Port](Port.fromInt(rawPort))
  yield port
end Network

