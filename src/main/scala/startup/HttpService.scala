package startup

import cats.data.Kleisli
import cats.effect.IO
import docker.{Docker, Images}
import org.http4s.{EntityDecoder, HttpRoutes, Request, Response}
import org.http4s.dsl.io.*
import org.http4s.circe.*
import python.ServiceRunner
import prediction.Prediction
import training.Input.Input
import training.Training
import categories.Category
import health.Health

object HttpService:
	val service: Kleisli[IO, Request[IO], Response[IO]] = HttpRoutes.of[IO] {
		case GET -> Root / "health" =>
			for
				response <- Health.getHealthStatus
				result <- Ok(response)
			yield result

		case req@POST -> Root / "predict" =>
			for
				image <- req.as[String]
				predictionString <- Docker.run(image, Images.Predict)
				response <- Prediction.fromString(predictionString)
				result <- Ok(response)
			yield result

		case req@POST -> Root / "train" =>
			for
				input <- req.as[Input]
				message <- ServiceRunner.train(input.image)
				response <- Training.fromString(message)
				result <- Ok(response)
			yield result

		case GET -> Root / "categories" => for
			categories <- Category.getCategories
			json <- Category.toJson(categories)
			result <- Ok(json)
		yield result
	}.orNotFound
end HttpService

