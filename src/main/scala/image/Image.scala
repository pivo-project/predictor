package image

import cats.effect.IO

import java.awt.Color
import java.io.File
import sys.process.*
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import scala.util.chaining.*

object Image:
	private def getImage(file: File): BufferedImage = ImageIO.read(file)

	private def makeColorSum(color: Color): Int =
		val red = (color.getRed * 0.299).toInt
		val green = (color.getGreen * 0.587).toInt
		val blue = (color.getBlue * 0.114).toInt
		red + green + blue
	end makeColorSum


	private def buildNewColor(image: BufferedImage, heightIndex: Int, widthIndex: Int): Color =
		val color = new Color(image.getRGB(widthIndex, heightIndex))

		val sum = makeColorSum(color)

		new Color(sum, sum, sum)
	end buildNewColor

	def grayscale(file: File): IO[Unit] =
		val image = getImage(file)

		val height = image.getHeight
		val width = image.getWidth

		for heightIndex <- 0 until height do
			for widthIndex <- 0 until width do
				val newColor = buildNewColor(image, heightIndex, widthIndex)

				image.setRGB(widthIndex, heightIndex, newColor.getRGB)
			end for
		end for

		IO(ImageIO.write(image, "jpg", file))
	end grayscale
end Image
