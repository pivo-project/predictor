package python

import cats.effect.IO
import common.{Base64Decoder, FileSystem}
import python.Functions
import python.System

object ServiceRunner:
	def train(image: String): IO[String] = for
		result <- System.buildCommand(Functions.Train, image)
	yield result

	def predict(image: String): IO[String] = for
		file <- FileSystem.makeFile(image)
		decodedImage <- Base64Decoder.decode(image)
		_ <- FileSystem.writeBytes(decodedImage, file)
		result <- System.buildCommand(Functions.Predict, file.getPath)
		_ <- FileSystem.removeFile(file.getPath)
	yield result
end ServiceRunner

