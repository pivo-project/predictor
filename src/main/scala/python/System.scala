package python

import cats.effect.IO
import common.Environment
import scala.util.chaining.*

object System:
	private def getModulePath = for
		module <- Environment.getValue[String]("MODULE_PY_PATH")
	yield module

	private def makePrefix(scriptPath: String, function: Functions) = IO.pure(s"from $scriptPath.${function.name} import *")

	private def build(prefix: String, input: String, function: Functions) = s"python -c \"$prefix; ${function.name}('$input')\""
		.pipe(IO.pure)

	def buildCommand(function: Functions, input: String): IO[String] = for
		scriptPath <- getModulePath
		prefix <- makePrefix(scriptPath, function)
		command <- build(prefix, input, function)
	yield command
end System
