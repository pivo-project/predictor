package python

enum Functions(val name: String):
	case Train extends Functions("train")
	case Predict extends Functions("predict")
end Functions

