package categories

import cats.effect.IO
import com.google.gson.internal.LinkedTreeMap
import io.circe.Json
import redis.Redis
import settings.Settings

import java.time.Instant
import java.util
import scala.jdk.CollectionConverters.*
import scala.util.chaining.*
import io.circe.syntax.*
import io.circe.generic.auto.*
import settings.Settings.PartialCategory

object Category:
	type ArrayList = util.ArrayList[Category]

	case class Category(name: String, createdAt: Long, previewImage: String)

	private def fromLinkedTreeMap(map: LinkedTreeMap[String, AnyVal]): Category =
		val name = map.get("name").asInstanceOf[String]
		val createdAt = map.get("createdAt").asInstanceOf[Double]
		val previewImage = map.get("previewImage").asInstanceOf[String]

		Category(name, createdAt.toLong, previewImage)
	end fromLinkedTreeMap

	private def fromPartial(partial: PartialCategory): Category =
		Category(name = partial.name, createdAt = Instant.now().getEpochSecond, previewImage = partial.previewImage)

	private def getDefaultCategories = for
		settings <- Settings.get()
		categories <- settings.categories.map(fromPartial).pipe(IO.pure)
	yield categories

	private def getDifferences(actual: List[Category], default: List[Category]): IO[Either[List[Category], List[Category]]] =
		if (actual.isEmpty)
			default pipe Left.apply pipe IO.apply
		else
			default.filter(defaultValue => !actual.exists(_.name == defaultValue.name)) pipe Right.apply pipe IO.apply
		end if
	end getDifferences

	private def arrayList(list: List[Category]) = new ArrayList(list.asJava)

	private def restore(differences: Either[List[Category], List[Category]]) = differences match
		case Right(value) =>
			if (value.isEmpty)
				IO.unit

			else
				value
					.map(Redis.append[Category]("categories", _))
					.reduce(_.race(_).map(_.merge))
		case Left(value) => Redis.set[ArrayList]("categories", arrayList(value))
			.flatMap(_ => IO.unit)

	def ensureDefault: IO[Unit] = for
		actual <- getCategories
		default <- getDefaultCategories
		differences <- getDifferences(actual, default)
		_ <- restore(differences)
	yield ()

	def getCategories: IO[List[Category]] = Redis.get[util.ArrayList[LinkedTreeMap[String, AnyVal]]]("categories")
		.map {
			case Some(arrayList) => arrayList.asScala.toList.map(fromLinkedTreeMap).distinctBy(_.name)
			case None => List.empty
		}

	def toJson(category: Category): IO[Json] = category.asJson.pipe(IO.pure)

	def toJson(categories: List[Category]): IO[Json] = categories.asJson.pipe(IO.pure)

end Category
