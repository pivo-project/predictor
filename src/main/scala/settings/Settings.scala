package settings

import cats.effect.IO
import com.moandjiezana.toml.Toml
import scala.util.chaining.*
import scala.jdk.CollectionConverters.*
import java.io.File
import java.util
import scala.io.Source

object Settings:
	private type HashMap = util.HashMap[String, String]
	private type ArrayList = util.ArrayList[HashMap]

	case class PartialCategory(name: String, previewImage: String)

	case class DockerImages(predict: String, train: String)

	case class Settings(categories: List[PartialCategory], dockerImages: DockerImages, predictionFailureThreshold: Double)

	private def getSettingsString = Source.fromResource("settings.toml")
		.getLines()
		.toList
		.mkString("\n")

	private def getFromMap[T, Y](map: Map[String, Object], key: String, convertor: Function[T, Y]): IO[Y] = map.get(key) match
		case Some(value) => IO.pure(convertor(value.asInstanceOf[T]))
		case None => IO.raiseError(new InternalError(s"Map does not have field $key"))

	private def getTomlObjectMap: IO[Map[String, Object]] = new Toml()
		.read(getSettingsString)
		.pipe(_.toMap.asScala.toMap)
		.pipe(IO.apply)

	// TODO: Use an implicit for the conversion
	private def hashMapToDockerImages(hashMap: HashMap): DockerImages =
		val casted = hashMap.asScala.toMap

		val predict = casted("predict")
		val train = casted("train")

		DockerImages(predict = predict, train = train)
	end hashMapToDockerImages


	private def arrayListToPartialCategories(arrayList: ArrayList): List[PartialCategory] =
		arrayList.asScala.toList.map(hashMap =>
			val casted = hashMap.asScala.toMap

			val name = casted("name")
			val previewImage = casted("preview_image")
			PartialCategory(name = name, previewImage = previewImage)
		)
	end arrayListToPartialCategories


	def get(): IO[Settings] = for
		objectMap <- getTomlObjectMap
		categories <- getFromMap[ArrayList, List[PartialCategory]](objectMap, "categories", arrayListToPartialCategories)
		dockerImages <- getFromMap[HashMap, DockerImages](objectMap, "docker_images", hashMapToDockerImages)
		predictionFailureThreshold <- getFromMap(objectMap, "prediction_failure_threshold", identity[Double])
	yield Settings(categories, dockerImages, predictionFailureThreshold)
	end get

end Settings

