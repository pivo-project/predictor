package minio

import cats.effect.IO
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.S3Object
import common.Environment

import util.chaining.*

object Client:
	implicit val bucketName: String = "videos"

	private def buildCredentials: IO[BasicAWSCredentials] = for
		accessKey <- Environment.getValue[String]("MINIO_ACCESS_KEY")
		secretKey <- Environment.getValue[String]("MINIO_SECRET_KEY")
	yield new BasicAWSCredentials(accessKey, secretKey)

	private def createClientInstance(host: String, port: Int, credentials: BasicAWSCredentials) = AmazonS3ClientBuilder
		.standard()
		.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(s"$host:$port", Regions.US_EAST_1.name()))
		.withPathStyleAccessEnabled(true)
		.withCredentials(new AWSStaticCredentialsProvider(credentials))
		.build()

	private def makeClient = for
		credentials <- buildCredentials
		host <- Environment.getValue[String]("MINIO_HOST")
		port <- Environment.getValue[Int]("MINIO_PORT")
	yield createClientInstance(host, port, credentials)

	def put(id: String, blob: Array[Byte])(implicit bucketName: String): IO[Unit] =
		makeClient.map(client => Setter.PutInput(client, bucketName, id, blob) pipe Setter.put)
	end put

	def getAll()(implicit bucketName: String): IO[List[S3Object]] =
		makeClient.map(client => Getter.GetAllInput(client, bucketName) pipe Getter.getAll)
	end getAll

end Client