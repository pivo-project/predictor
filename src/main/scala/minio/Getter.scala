package minio

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.{GetObjectRequest, ListObjectsRequest, S3Object, S3ObjectSummary}

import scala.jdk.CollectionConverters.*
import java.util

object Getter:
	case class GetAllInput(client: AmazonS3, bucketName: String)

	def getAll(input: GetAllInput): List[S3Object] =
		var listing = input.client.listObjects(input.bucketName)
		val summaries = listing.getObjectSummaries
		val objects: List[S3Object] = List()

		while listing.isTruncated do
			listing = input.client.listNextBatchOfObjects(listing)
			summaries.addAll(listing.getObjectSummaries)
		end while

		summaries.forEach(summary => 
			val s3Object = input.client.getObject(new GetObjectRequest(summary.getBucketName, summary.getKey)) 
			objects.appended(s3Object)
		)
		
		objects
	end getAll
end Getter
