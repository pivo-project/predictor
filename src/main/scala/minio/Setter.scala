package minio

import cats.effect.IO
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata

import java.io.ByteArrayInputStream

object Setter:
	case class PutInput(client: AmazonS3, bucketName: String, id: String, blob: Array[Byte])

	private def createBucketIfNotExisting(input: PutInput): IO[Unit] =
		if !input.client.doesBucketExistV2(input.bucketName) then
			IO(input.client.createBucket(input.bucketName))
		else
			IO.unit
		end if
	end createBucketIfNotExisting

	private def putObject(input: PutInput): IO[Unit] =
		val inputStream = new ByteArrayInputStream(input.blob)
		val metadata = new ObjectMetadata()
		metadata.setContentLength(inputStream.available())

		IO(input.client.putObject(input.bucketName, input.id, inputStream, metadata)) >> IO(inputStream.close())
	end putObject

	def put(input: PutInput): IO[Unit] =
		createBucketIfNotExisting(input) >> putObject(input)
	end put

end Setter
