package health

import cats.effect.IO
import io.circe.Json
import io.circe.syntax.*
import scala.util.chaining._
import io.circe.generic.auto._

object Health:
	private case class Status(status: String)

	def getHealthStatus: IO[Json] = Status("up")
		.asJson
		.pipe(IO.pure)
end Health
