package prediction

import categories.Category.{Category, getCategories}
import settings.Settings.{Settings, get}
import cats.effect.IO
import io.circe.{Encoder, Json}
import io.circe.literal.json
import io.circe.syntax.*

import scala.util.chaining.*
import io.circe.generic.auto.*

object Prediction:
	private def listFromResponse(response: String): Array[Double] = response
		.replace("[", "")
		.replace("]", "")
		.split(',')
		.map(_.strip)
		.map(_.toDouble)

	private def getSecondMax(weights: Array[Double]): Double =
		weights.sorted.reverse.apply(1)
	end getSecondMax

	private def getIndexOfMaxValue(weights: Array[Double], settings: Settings): Either[Unit, Int] =
		val max = weights.max
		val secondMax = getSecondMax(weights)
		val failureThreshold = settings.predictionFailureThreshold

		if (max - secondMax) < failureThreshold then
			Left(IO.unit)
		else
			Right(weights.indexOf(max))
		end if
	end getIndexOfMaxValue

	private def matchResponse(response: String, categories: List[Category], settings: Settings): IO[Category] =
		println("Got tensorflow response:")
		println(response)
		val weights = listFromResponse(response)

		getIndexOfMaxValue(weights, settings) match
			case Left(_) => Category("unknown", 0, "").pipe(IO.pure)
			case Right(predictedPosition) => categories.apply(predictedPosition).pipe(IO.pure)
	end matchResponse

	private def getCategory(response: String): IO[Category] =
		for
			settings <- get()
			categories <- getCategories
			category <- matchResponse(response, categories, settings)
		yield category
	end getCategory

	private def toJson(value: Category): IO[Json] = value
		.asJson
		.pipe(IO.pure)

	def fromString(response: String): IO[Json] =
		for
			category <- getCategory(response)
			json <- toJson(category)
		yield json
	end fromString

end Prediction
