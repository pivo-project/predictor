package startup

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.effect.unsafe.implicits.global
import com.comcast.ip4s.{Port, Ipv4Address}

import scala.language.postfixOps

class NetworkTest extends AnyFlatSpec with Matchers {

	behavior of "NetworkTest"

	it should "get target port" in {
		val expected = Port.fromInt(8080).get

		Network.getPort
			.unsafeRunSync() shouldBe expected
	}

	it should "get target host" in {
		val expected = Ipv4Address.fromString("0.0.0.0").get

		Network.getHost
			.unsafeRunSync() shouldBe expected
	}

}
