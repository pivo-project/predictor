package python

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.effect.unsafe.implicits.global

import scala.language.postfixOps

class SystemTest extends AnyFlatSpec with Matchers {
	behavior of "SystemTest"

	it should "execute predict function" in {
		val function = Functions.Predict
		val input = "TEST"
		val expected = s"python -c \"from python.test.predict import *; predict('$input')\""

		System.buildCommand(function, input)
			.unsafeRunSync() shouldBe expected
	}

	it should "execute train function" in {
		val function = Functions.Train
		val input = "TEST"
		val expected = s"python -c \"from python.test.train import *; train('$input')\""

		System.buildCommand(function, input)
			.unsafeRunSync() shouldBe expected
	}
}