package training

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.effect.unsafe.implicits.global
import io.circe.literal.json

import scala.language.postfixOps

class TrainingTest extends AnyFlatSpec with Matchers {

	behavior of "TrainingTest"

	it should "create json from string" in {
		val input = "TEST"
		val expected = json""" { "message":  $input} """

		Training.fromString(input)
			.unsafeRunSync() shouldBe expected
	}

}
