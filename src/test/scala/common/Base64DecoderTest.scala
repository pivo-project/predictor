package common

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.effect.unsafe.implicits.global

import scala.language.postfixOps

class Base64DecoderTest extends AnyFlatSpec with Matchers {
	behavior of "Base64DecoderTest"

	it should "decode base64" in {
		val input = "YmFzZTY0aW5wdXQ="
		val expected = "base64input"

		Base64Decoder
			.decode(input)
			.map(bytes => new String(bytes))
			.unsafeRunSync() shouldBe expected
	}
}
