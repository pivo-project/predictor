package common

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import cats.effect.unsafe.implicits.global

import scala.language.postfixOps

class EnvironmentTest extends AnyFlatSpec with Matchers {

	behavior of "EnvironmentTest"

	it should "get string value" in {
		val input = "HOST"
		val expected = "0.0.0.0"

		Environment.getValue[String](input)
			.unsafeRunSync() shouldBe expected
	}

	it should "get int value" in {
		val input = "PORT"
		val expected = 8080

		Environment.getValue[Int](input)
			.unsafeRunSync() shouldBe expected
	}

}
