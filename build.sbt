val scala3Version = "3.2.2"

val http4sVersion = "1.0.0-M37"

lazy val root = project
	.in(file("."))
	.settings(
		name := "predictor",
		version := "0.0.1",

		scalaVersion := scala3Version,

		libraryDependencies ++= Seq(
			"org.http4s" %% "http4s-dsl" % http4sVersion,
			"org.http4s" %% "http4s-ember-server" % http4sVersion,
			"org.http4s" %% "http4s-circe" % http4sVersion,
			"io.circe" %% "circe-generic" % "0.14.5",
			"io.circe" %% "circe-literal" % "0.14.5",
			"ch.qos.logback" % "logback-classic" % "1.4.6" % Runtime,
			"redis.clients" % "jedis" % "4.3.1",
			"org.scalactic" %% "scalactic" % "3.2.15" % Test,
			"org.scalatest" %% "scalatest" % "3.2.15" % Test,
			"org.typelevel" %% "cats-effect-testing-scalatest" % "1.4.0" % Test,
			"com.moandjiezana.toml" % "toml4j" % "0.7.2",
			"com.amazonaws" % "aws-java-sdk" % "1.12.429"
		),

		Test / fork := true,

		Test / envFileName := ".env.test",

		Test / envVars := (Test / envFromFile).value,

		exportJars := true,
		assembly / assemblyJarName := "predictor.jar",

		ThisBuild / assemblyMergeStrategy := {
			case PathList("META-INF", _*) => MergeStrategy.discard
			case _ => MergeStrategy.first
		}
	)
