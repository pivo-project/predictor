FROM registry.gitlab.com/pivo-project/predictor:from

WORKDIR /app
COPY . .

RUN sbt clean assembly

CMD ["java", "-jar", "./target/scala-3.2.2/predictor.jar"]
