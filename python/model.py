import time
import src.predict as Predictor

def train(image):
    time.sleep(10)

    print(f"model retrained with image {image}")

def predict(imagePath):
    Predictor.predict(imagePath)

