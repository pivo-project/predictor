using Flux
using Flux.Data: DataLoader
using Flux: @epochs, onecold, onehotbatch, logitcrossentropy, throttle
using MLDatasets
using Images
using ImageClassificationDatasets
using MLDataPattern: stratifiedobs, shuffleobs
using BSON: @save

batch_size = 8
img_height = 180
img_width = 101
epochs = 10

function load_data(data_dir)
    X, y = loadfolder(data_dir, image_size = (img_height, img_width))
    labels = sort(unique(y))
    Y = onehotbatch(y, labels)
    data = [(cat(float.(X[i])..., dims = 4), Y[:,i]) for i in partition(1:length(y), batch_size)]
    train_indices, val_indices = stratifiedobs((y, 0.8), pids=1)
    return shuffleobs(data[train_indices]), data[val_indices], labels
end

function train_new_model(data_dir)
    train_data, val_data, class_names = load_data(data_dir)
    num_classes = length(class_names)

    model = Chain(
        Conv((3,3), 3=>16, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3,3), 16=>32, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3,3), 32=>64, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3,3), 64=>128, pad=(1,1), relu),
        MaxPool((2,2)),
        Flux.flatten,
        Dense(128*img_height*img_width÷(2^4)^2, 256, softmax),
        Dense(256, num_classes)
    )
    
    loss(x, y) = logitcrossentropy(model(x), y)
    accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

    evalcb = throttle(() -> @show(accuracy(val_data...)), 10)
    opt = ADAM()

    @epochs epochs Flux.train!(loss, params(model), train_data, opt, cb = evalcb)

    @save "saved_model/model.bson" model
end

