import tensorflow as tf

import cv2

img_height = 101
img_width = 180

def predict(image_path):
    model = tf.keras.models.load_model('/app/python/saved_model/model.h5')
    image = cv2.imread(image_path)
    image = cv2.resize(image, (img_width, img_height))

    image = image[None, ...]

    class_prob = model.predict(image)
    print(class_prob[0].tolist())
