#!/bin/sh

docker build -t registry.gitlab.com/pivo-project/predictor:build --build-arg module_py_path="$1" -f build.Dockerfile .
