#!/bin/sh

docker build -t registry.gitlab.com/pivo-project/predictor:test -f test.Dockerfile .
docker run registry.gitlab.com/pivo-project/predictor:test
