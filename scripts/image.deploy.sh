#!/bin/sh

echo "$2" | docker login --username "$1" --password-stdin registry.gitlab.com
docker push registry.gitlab.com/pivo-project/predictor:build
