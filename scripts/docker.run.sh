#!/bin/sh

docker run --env-file .env -v /var/run/docker.sock:/var/run/docker.sock -p 8080:8080 registry.gitlab.com/pivo-project/predictor:build
