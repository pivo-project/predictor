# The Pivo Predictor
The prediction service of the Pivo app. It's built in Scala using http4s and Cats Effect.

## Usage
* Clone this repository
* Run: `docker compose up`

## Endpoints
* GET `/` -> test endpoint that simply starts a fiber which will echo "Hello World"
* POST `/predict` -> receives a base64 representation of an image (as plain text) and will return the predicted category of said image
* POST `/train` -> will retrain the model with the given category (NOT FULLY IMPLEMENTED)
* GET `/categories` -> will fetch the currently registered categories

## Dependent services
* Minio -> used to store the videos which are used as training material for the Tensorflow model
* Tensorflow -> docker container in which the actual Tensorflow commands are being executed
* Database -> the Redis database of the Pivo project

## Prediction flow
Here are the steps that are executed when predicting an image's category:
* The image is received as a base64 string
* The string is decoded and the image is saved under `/temp/<random_guid>`
* The image is transformed to grayscale
* A `docker exec` command is sent to the Tensorflow container which runs the prediction code. It's output has the following shape: `[0.0, 1.0, 0.0]`, representing the probability that the image
is part of a category (sorted alphabetically)
* The existing categories are fetched from the database and then matched with the Tensorflow result 
* The "statistically most likely" (the value that is biggest by _at least_ a given threshold) result is selected
* If no "statistically most likely" result is found, an _Unknown_ category is returned
* Otherwise, the resulting category is returned

## The retraining flow
TBD

## Notes
* Endpoints can apply gzip compression on their responses if the `Accept-Encoding: gzip` header is present in the request

